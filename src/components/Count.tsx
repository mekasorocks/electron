import * as React from 'react';

interface IProps {}
interface IState {
    wert: number
}
class Count extends React.Component<IProps, IState> {
    constructor(props: IProps, state: IState) {
        super(props, state);
        this.state = ({
            wert: 0
        });
    }

    addierenHandler = () => {
        let wert: number = this.state.wert;
        wert++;
        this.setState({
            wert: wert
        });
    }

    subtrahierenHandler = () => {
        let wert: number = this.state.wert;
        wert--;
        this.setState({
            wert: wert
        });
    }

    render() {
        return (
            <div>
                <div>{this.state.wert}</div>
                <div>
                    <input type="button" value="+" onClick={this.addierenHandler} />
                    <input type="button" value="-" onClick={this.subtrahierenHandler} />
                </div>
            </div>
        )
    }
}
export default Count;